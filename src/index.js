import React from "react";
import ReactDOM from "react-dom";
import CustomMentionEditor from "./PostEditor/SimpleMentionEditor";
import "./styles.css";
class Post extends React.Component {
  constructor(props) {
    super(props);
    this.mentionsRef = React.createRef();
  }
 
  render() {
    // console.log(this.mentionsRef,'s')
    return (
      <div className="container">
        <div className="PostEditor__container">
          <div className="PostEditor">
            <CustomMentionEditor  />
          </div>
        </div>
      </div>
    );
  }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<Post />, rootElement);
