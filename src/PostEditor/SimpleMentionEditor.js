import React, { Component } from "react";
import Editor, { createEditorStateWithText } from "draft-js-plugins-editor";
import createEmojiPlugin from "draft-js-emoji-plugin";
import createMentionPlugin, {
  defaultSuggestionsFilter
} from "draft-js-mention-plugin";
import "draft-js-mention-plugin/lib/plugin.css";
import "draft-js-emoji-plugin/lib/plugin.css";
import { convertToRaw } from "draft-js";
import mentions from "./data";

const emojiPlugin = createEmojiPlugin();
const { EmojiSuggestions, EmojiSelect } = emojiPlugin;
const mentionPlugin = createMentionPlugin();
const { MentionSuggestions } = mentionPlugin;
const plugins = [emojiPlugin, mentionPlugin];
const text = "";


export default class CustomToolbarEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: createEditorStateWithText(text),
      suggestions: mentions,
      mention:'',
      open:false,
      clickOpen:false
    };
  }

  onChange = editorState => {
    this.setState({
      editorState
    });
  };

  onOpenChange = (open) => {
    this.setState({ open: !open });
  };

  setOpenClick = (clickOpen) => {
    this.setState({ clickOpen: !clickOpen });
    console.log('clickOpen', clickOpen)
  };

  focus = () => {
    this.editor.focus();
  };

  onAddMention = (mention) => {
   
    console.log('mention', mention)
  };

  onSearchChange = ({ value }) => {
    this.setState({
      suggestions: defaultSuggestionsFilter(value, mentions)
    });
  };

  setOpen = ({openMention}) => {
    this.setState({
      openMention: !openMention
    });
  };

  renderContentAsRawJs = () => {
    const contentState = this.state.editorState.getCurrentContent();
    const raw = convertToRaw(contentState);
    const list = JSON.stringify(raw.entityMap, null, 2);
    // const obj1 = list.substring(1,list.length-1);
    const obj = JSON.parse(list);
    const res = [];
    for(var i in obj)
          res.push(obj[i]);
              
    console.log('list', res)
        // eslint-disable-next-line 
    if(res.map(item => item.type)  == 'mention'){
      const print =  res.map(item => item.data.mention.ID)              
      console.log(print,'list ID')
      
      return print
    }
   
  }
  render() {
    const { clickOpen, open} = this.state
    console.log('clickOpen', clickOpen)
    console.log('open', open)
    return (
      <div className={'editor'} onClick={this.focus}>
        <Editor
          editorState={this.state.editorState}
          onChange={this.onChange}

          plugins={plugins}
          ref={element => {
            this.editor = element;
          }}
        />
        <button onClick={this.setOpenClick}>@</button>
        <MentionSuggestions
          open={this.state.open || this.state.clickOpen}
          onOpenChange={this.onOpenChange}
          onSearchChange={this.onSearchChange}
          suggestions={this.state.suggestions}
          onAddMention={this.onAddMention}
        />
        <EmojiSuggestions />
        <span className={'options'}>
          <EmojiSelect closeOnEmojiSelect />
        </span>
        <div>
         
          <pre id="raw-display">[{this.renderContentAsRawJs()}]</pre>
          
        </div>
      </div>
    );
  }
}
